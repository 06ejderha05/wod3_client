#include "PythonrootlibManager.h"
#include "Python.h"
#ifdef _DEBUG
#pragma comment (lib, "rootlib_d.lib")
#else
#pragma comment (lib, "rootlib.lib")
#endif

struct rootlib_SMethodDef
{
	char* func_name;
	void(*func)();
};

PyMODINIT_FUNC initcolorInfo();
PyMODINIT_FUNC initconsoleModule();
PyMODINIT_FUNC initconstInfo();
PyMODINIT_FUNC initdebugInfo();
PyMODINIT_FUNC initdragon_soul_refine_settings();
PyMODINIT_FUNC initeffecttable();
PyMODINIT_FUNC initemotion();
PyMODINIT_FUNC initexception();
PyMODINIT_FUNC initgame();
PyMODINIT_FUNC initinterfaceModule();
PyMODINIT_FUNC initintroCreate();
PyMODINIT_FUNC initintroEmpire();
PyMODINIT_FUNC initintroLoading();
PyMODINIT_FUNC initintroLogin();
PyMODINIT_FUNC initintroLogo();
PyMODINIT_FUNC initintroSelect();
PyMODINIT_FUNC inititemNames();
PyMODINIT_FUNC initlocaleInfo();
PyMODINIT_FUNC initmouseModule();
PyMODINIT_FUNC initmusicInfo();
PyMODINIT_FUNC initnetworkModule();
PyMODINIT_FUNC initplayerSettingModule();
PyMODINIT_FUNC initPrototype();
PyMODINIT_FUNC initRootLibCythonizer();
PyMODINIT_FUNC initserverCommandParser();
PyMODINIT_FUNC initserverInfo();
PyMODINIT_FUNC initstringCommander();
PyMODINIT_FUNC initswitchbot();
PyMODINIT_FUNC initsystem();
PyMODINIT_FUNC inittest_affect();
PyMODINIT_FUNC initui();
PyMODINIT_FUNC inituiAffectShower();
PyMODINIT_FUNC inituiAttachMetin();
PyMODINIT_FUNC inituiattrdialog();
PyMODINIT_FUNC inituiAuction();
PyMODINIT_FUNC inituiaurayukselt();
PyMODINIT_FUNC inituiAutoban();
PyMODINIT_FUNC inituiautohunt();
PyMODINIT_FUNC inituibonus();
PyMODINIT_FUNC inituibonuschanger();
PyMODINIT_FUNC inituiCandidate();
PyMODINIT_FUNC inituichangelook();
PyMODINIT_FUNC inituiCharacter();
PyMODINIT_FUNC inituiChat();
PyMODINIT_FUNC inituichatblock();
PyMODINIT_FUNC inituiChestDrop();
PyMODINIT_FUNC inituiCommon();
PyMODINIT_FUNC inituicrafting();
PyMODINIT_FUNC inituiCube();
PyMODINIT_FUNC inituiDamageTop();
PyMODINIT_FUNC inituiDragonSoul();
PyMODINIT_FUNC inituiduel();
PyMODINIT_FUNC inituiEquipmentDialog();
PyMODINIT_FUNC inituieventboard();
PyMODINIT_FUNC inituieventspanel();
PyMODINIT_FUNC inituiEx();
PyMODINIT_FUNC inituiExchange();
PyMODINIT_FUNC inituiGameButton();
PyMODINIT_FUNC inituiGameOption();
PyMODINIT_FUNC inituiGift();
PyMODINIT_FUNC inituigmboard();
PyMODINIT_FUNC inituiGuild();
PyMODINIT_FUNC inituiHelp();
PyMODINIT_FUNC inituiInventory();
PyMODINIT_FUNC inituikokot();
PyMODINIT_FUNC inituiLocalization();
PyMODINIT_FUNC inituimaintenance();
PyMODINIT_FUNC inituiMapNameShower();
PyMODINIT_FUNC inituiMessenger();
PyMODINIT_FUNC inituiMiniGame();
PyMODINIT_FUNC inituiMiniGameFishEvent();
PyMODINIT_FUNC inituiMiniMap();
PyMODINIT_FUNC inituimsgpopup();
PyMODINIT_FUNC inituiNewShop();
PyMODINIT_FUNC inituiOption();
PyMODINIT_FUNC inituiParty();
PyMODINIT_FUNC inituiPhaseCurtain();
PyMODINIT_FUNC inituiPickMoney();
PyMODINIT_FUNC inituiPlayerGauge();
PyMODINIT_FUNC inituiPointReset();
PyMODINIT_FUNC inituipricechecker();
PyMODINIT_FUNC inituiPrivateShopBuilder();
PyMODINIT_FUNC inituiprivateshopsearch();
PyMODINIT_FUNC inituiQuest();
PyMODINIT_FUNC inituiRefine();
PyMODINIT_FUNC inituiRestart();
PyMODINIT_FUNC inituiSafebox();
PyMODINIT_FUNC inituiScriptLocale();
PyMODINIT_FUNC inituiselectdecoration();
PyMODINIT_FUNC inituiselectitem();
PyMODINIT_FUNC inituiSelectLanguage();
PyMODINIT_FUNC inituiSelectMusic();
PyMODINIT_FUNC inituiselectwarehousetype();
PyMODINIT_FUNC inituiShop();
PyMODINIT_FUNC inituiskillcolor();
PyMODINIT_FUNC inituiskillcolorquest();
PyMODINIT_FUNC inituislotmachine();
PyMODINIT_FUNC inituiSystem();
PyMODINIT_FUNC inituiSystemOption();
PyMODINIT_FUNC inituiTarget();
PyMODINIT_FUNC inituiTaskBar();
PyMODINIT_FUNC inituiteleport();
PyMODINIT_FUNC inituiTip();
PyMODINIT_FUNC inituititlecolor();
PyMODINIT_FUNC inituititlelist();
PyMODINIT_FUNC inituitombola();
PyMODINIT_FUNC inituiToolTip();
PyMODINIT_FUNC inituiUploadMark();
PyMODINIT_FUNC inituiUtils();
PyMODINIT_FUNC inituiWeb();
PyMODINIT_FUNC inituiWhisper();
PyMODINIT_FUNC initutils();

rootlib_SMethodDef rootlib_init_methods[] =
{
	{ "colorInfo", initcolorInfo },
	{ "consoleModule", initconsoleModule },
	{ "constInfo", initconstInfo },
	{ "debugInfo", initdebugInfo },
	{ "dragon_soul_refine_settings", initdragon_soul_refine_settings },
	{ "effecttable", initeffecttable },
	{ "emotion", initemotion },
	{ "exception", initexception },
	{ "game", initgame },
	{ "interfaceModule", initinterfaceModule },
	{ "introCreate", initintroCreate },
	{ "introEmpire", initintroEmpire },
	{ "introLoading", initintroLoading },
	{ "introLogin", initintroLogin },
	{ "introLogo", initintroLogo },
	{ "introSelect", initintroSelect },
	{ "itemNames", inititemNames },
	{ "localeInfo", initlocaleInfo },
	{ "mouseModule", initmouseModule },
	{ "musicInfo", initmusicInfo },
	{ "networkModule", initnetworkModule },
	{ "playerSettingModule", initplayerSettingModule },
	{ "Prototype", initPrototype },
	{ "RootLibCythonizer", initRootLibCythonizer },
	{ "serverCommandParser", initserverCommandParser },
	{ "serverInfo", initserverInfo },
	{ "stringCommander", initstringCommander },
	{ "switchbot", initswitchbot },
	{ "system", initsystem },
	{ "test_affect", inittest_affect },
	{ "ui", initui },
	{ "uiAffectShower", inituiAffectShower },
	{ "uiAttachMetin", inituiAttachMetin },
	{ "uiattrdialog", inituiattrdialog },
	{ "uiAuction", inituiAuction },
	{ "uiaurayukselt", inituiaurayukselt },
	{ "uiAutoban", inituiAutoban },
	{ "uiautohunt", inituiautohunt },
	{ "uibonus", inituibonus },
	{ "uibonuschanger", inituibonuschanger },
	{ "uiCandidate", inituiCandidate },
	{ "uichangelook", inituichangelook },
	{ "uiCharacter", inituiCharacter },
	{ "uiChat", inituiChat },
	{ "uichatblock", inituichatblock },
	{ "uiChestDrop", inituiChestDrop },
	{ "uiCommon", inituiCommon },
	{ "uicrafting", inituicrafting },
	{ "uiCube", inituiCube },
	{ "uiDamageTop", inituiDamageTop },
	{ "uiDragonSoul", inituiDragonSoul },
	{ "uiduel", inituiduel },
	{ "uiEquipmentDialog", inituiEquipmentDialog },
	{ "uieventboard", inituieventboard },
	{ "uieventspanel", inituieventspanel },
	{ "uiEx", inituiEx },
	{ "uiExchange", inituiExchange },
	{ "uiGameButton", inituiGameButton },
	{ "uiGameOption", inituiGameOption },
	{ "uiGift", inituiGift },
	{ "uigmboard", inituigmboard },
	{ "uiGuild", inituiGuild },
	{ "uiHelp", inituiHelp },
	{ "uiInventory", inituiInventory },
	{ "uikokot", inituikokot },
	{ "uiLocalization", inituiLocalization },
	{ "uimaintenance", inituimaintenance },
	{ "uiMapNameShower", inituiMapNameShower },
	{ "uiMessenger", inituiMessenger },
	{ "uiMiniGame", inituiMiniGame },
	{ "uiMiniGameFishEvent", inituiMiniGameFishEvent },
	{ "uiMiniMap", inituiMiniMap },
	{ "uimsgpopup", inituimsgpopup },
	{ "uiNewShop", inituiNewShop },
	{ "uiOption", inituiOption },
	{ "uiParty", inituiParty },
	{ "uiPhaseCurtain", inituiPhaseCurtain },
	{ "uiPickMoney", inituiPickMoney },
	{ "uiPlayerGauge", inituiPlayerGauge },
	{ "uiPointReset", inituiPointReset },
	{ "uipricechecker", inituipricechecker },
	{ "uiPrivateShopBuilder", inituiPrivateShopBuilder },
	{ "uiprivateshopsearch", inituiprivateshopsearch },
	{ "uiQuest", inituiQuest },
	{ "uiRefine", inituiRefine },
	{ "uiRestart", inituiRestart },
	{ "uiSafebox", inituiSafebox },
	{ "uiScriptLocale", inituiScriptLocale },
	{ "uiselectdecoration", inituiselectdecoration },
	{ "uiselectitem", inituiselectitem },
	{ "uiSelectLanguage", inituiSelectLanguage },
	{ "uiSelectMusic", inituiSelectMusic },
	{ "uiselectwarehousetype", inituiselectwarehousetype },
	{ "uiShop", inituiShop },
	{ "uiskillcolor", inituiskillcolor },
	{ "uiskillcolorquest", inituiskillcolorquest },
	{ "uislotmachine", inituislotmachine },
	{ "uiSystem", inituiSystem },
	{ "uiSystemOption", inituiSystemOption },
	{ "uiTarget", inituiTarget },
	{ "uiTaskBar", inituiTaskBar },
	{ "uiteleport", inituiteleport },
	{ "uiTip", inituiTip },
	{ "uititlecolor", inituititlecolor },
	{ "uititlelist", inituititlelist },
	{ "uitombola", inituitombola },
	{ "uiToolTip", inituiToolTip },
	{ "uiUploadMark", inituiUploadMark },
	{ "uiUtils", inituiUtils },
	{ "uiWeb", inituiWeb },
	{ "uiWhisper", inituiWhisper },
	{ "utils", initutils },
	{ NULL, NULL },
};

static PyObject* rootlib_isExist(PyObject *self, PyObject *args)
{
	char* func_name;

	if (!PyArg_ParseTuple(args, "s", &func_name))
		return NULL;

	for (int i = 0; NULL != rootlib_init_methods[i].func_name; i++)
	{
		if (0 == _stricmp(rootlib_init_methods[i].func_name, func_name))
		{
			return Py_BuildValue("i", 1);
		}
	}
	return Py_BuildValue("i", 0);
}

static PyObject* rootlib_moduleImport(PyObject *self, PyObject *args)
{
	char* func_name;

	if (!PyArg_ParseTuple(args, "s", &func_name))
		return NULL;

	for (int i = 0; NULL != rootlib_init_methods[i].func_name; i++)
	{
		if (0 == _stricmp(rootlib_init_methods[i].func_name, func_name))
		{
			rootlib_init_methods[i].func();
			if (PyErr_Occurred())
				return NULL;
			PyObject* m = PyDict_GetItemString(PyImport_GetModuleDict(), rootlib_init_methods[i].func_name);
			if (m == NULL) {
				PyErr_SetString(PyExc_SystemError,
					"dynamic module not initialized properly");
				return NULL;
			}
			Py_INCREF(m);
			return Py_BuildValue("S", m);
		}
	}
	return NULL;
}

static PyObject* rootlib_getList(PyObject *self, PyObject *args)
{
	int iTupleSize = 0;
	while (NULL != rootlib_init_methods[iTupleSize].func_name) { iTupleSize++; }

	PyObject* retTuple = PyTuple_New(iTupleSize);
	for (int i = 0; NULL != rootlib_init_methods[i].func_name; i++)
	{
		PyObject* retSubString = PyString_FromString(rootlib_init_methods[i].func_name);
		PyTuple_SetItem(retTuple, i, retSubString);
	}
	return retTuple;
}

void initrootlibManager()
{
	static struct PyMethodDef methods[] =
	{
		{ "isExist", rootlib_isExist, METH_VARARGS },
		{ "moduleImport", rootlib_moduleImport, METH_VARARGS },
		{ "getList", rootlib_getList, METH_VARARGS },
		{ NULL, NULL },
	};

	PyObject* m;
	m = Py_InitModule("rootlib", methods);
}
