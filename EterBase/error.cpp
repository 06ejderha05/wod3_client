#include "StdAfx.h"

#include <stdio.h>
#include <time.h>
#include <winsock.h>
#include <imagehlp.h>

FILE * fException;

#include <crash_reporter/CrashRpt.h>
#include "../UserInterface/Locale.h"

//#ifdef ENABLE_CRASH_REPORT
void SetEterExceptionHandler()
{
	/*
	std::string realLang;
	FILE* fp = fopen("lang.cfg", "rt");

	if (fp)
	{
		char	line[256];
		char	name[256];
		if (fgets(line, sizeof(line)-1, fp))
		{
			line[sizeof(line)-1] = '\0';
			if (sscanf(line, "%s", name) == 1)
			{
				realLang = name;
			}

		}
		fclose(fp);
	}
	*/

	CR_INSTALL_INFO info;
	memset(&info, 0, sizeof(CR_INSTALL_INFO));
	info.cb = sizeof(CR_INSTALL_INFO);  
	info.pszAppName = "WoDMT2"; // Define application name.
	info.pszAppVersion = "1.1.0"; // Define application version.
	/*
	if (!strcmpi(realLang.c_str(), "cz"))
		info.pszLangFilePath = "sys/crashrpt_lang_CS.ini";
	else
		info.pszLangFilePath = "sys/crashrpt_lang.ini";
	*/
	info.pszLangFilePath = "sys/crashrpt_lang_CS.ini";

	// URL for sending error reports over HTTP.
	info.pszUrl = "https://wodmt2.cz/crashrpt/report.php";                    
	// Install all available exception handlers.
	info.dwFlags |= CR_INST_ALL_POSSIBLE_HANDLERS; 
	// Use binary encoding for HTTP uploads (recommended).
	// info.dwFlags |= CR_INST_HTTP_BINARY_ENCODING;     --Deprecated
	info.dwFlags |= CR_INST_SEND_QUEUED_REPORTS; 
	// Provide privacy policy URL
	info.pszPrivacyPolicyURL = "https://wodmt2.cz/crashrpt/policy.html";
	info.dwFlags |= CR_INST_APP_RESTART; // Restarts the app after crash

	info.uPriorities[CR_HTTP] = 0;  // First try send report over HTTP 
	info.uPriorities[CR_SMTP] = CR_NEGATIVE_PRIORITY;  // Disable sending over SMTP - use only HTTP 

	int nResult = crInstall(&info);
	if(nResult!=0)
	{
		TCHAR buff[256];
		crGetLastErrorMsg(buff, 256);
		MessageBox(NULL, buff, "crInstall error", MB_OK);
		return;
	}

	crAddFile2("syserr.txt", NULL, "ErrorLog File", CR_AF_MAKE_FILE_COPY);
	return;
}
//#endif

//#ifdef ENABLE_CRASH_REPORT
void RemoveEterException()
{
	crUninstall();
	return;
}
//#endif
