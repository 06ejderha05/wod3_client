#pragma once

#define APP_VERSION "1000"

//////////////////////////////////////////////////////////////////////////
// ### Default Ymir Macros ###
#define LOCALE_SERVICE_EUROPE
#define ENABLE_COSTUME_SYSTEM
//#define ENABLE_ENERGY_SYSTEM
#define ENABLE_DRAGON_SOUL_SYSTEM
#define ENABLE_NEW_EQUIPMENT_SYSTEM
// ### Default Ymir Macros ###
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// ### New From LocaleInc ###
#define ENABLE_PACK_GET_CHECK
#define ENABLE_CANSEEHIDDENTHING_FOR_GM
#define ENABLE_PROTOSTRUCT_AUTODETECT

//#define ENABLE_PLAYER_PER_ACCOUNT5
#define ENABLE_LEVEL_IN_TRADE
#define ENABLE_DICE_SYSTEM
#define ENABLE_EXTEND_INVEN_SYSTEM
#define ENABLE_LVL115_ARMOR_EFFECT

#define WJ_SHOW_MOB_INFO
#ifdef WJ_SHOW_MOB_INFO
#define ENABLE_SHOW_MOBAIFLAG
#define ENABLE_SHOW_MOBLEVEL
#endif
// ### New From LocaleInc ###
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// ### From GameLib ###
//#define ENABLE_WOLFMAN_CHARACTER

#define ENABLE_MAGIC_REDUCTION_SYSTEM
//#define ENABLE_MOUNT_COSTUME_SYSTEM
#define ENABLE_WEAPON_COSTUME_SYSTEM
// ### From GameLib ###
//////////////////////////////////////////////////////////////////////////

// NEW NEW NEW NEW NEW NEW NEW
#define ENABLE_CANBLOCK_PLAYER_SYSTEM
#define ENABLE_PVP_ADVANCED
//#define EQUIP_ENABLE_VIEW_SASH ///* If you not have sash system implemented delete this line. #define EQUIP_ENABLE_VIEW_SASH *///

#define ENABLE_HIGHLIGHT_SYSTEM
#define ENABLE_VIEW_TARGET_PLAYER_HP
#define ENABLE_VIEW_TARGET_DECIMAL_HP
#define ENABLE_MOB_SCALE_SYSTEM
#define ENABLE_SEND_TARGET_INFO
#define ENABLE_TAB_NEXT_TARGET
#define WJ_SPLIT_INVENTORY_SYSTEM
//#define ENABLE_CHANGELOOK_SYSTEM // implemented without ifdef
//#define STATRAK_SYSTEM // implemented without ifdef
//#define KYGN_AURA // implemented without ifdef
#define ENABLE_DISCORD_RPC
//#define ENABLE_KORAY_BETASHIELD
#define __ENABLE_SHAMAN_ATTACK_FIX__
