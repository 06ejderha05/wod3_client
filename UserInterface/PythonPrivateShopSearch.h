#pragma once

class CPythonPrivateShopSearch : public CSingleton<CPythonPrivateShopSearch>
{
	public:
		struct TSearchItemData : TItemData
		{
			DWORD vid;
			long long price;
			BYTE Cell;
		};

		//using TItemInstanceVector = std::vector<TSearchItemData>;
		typedef std::vector<TSearchItemData> TItemInstanceVector;


	public:
		CPythonPrivateShopSearch();
		virtual ~CPythonPrivateShopSearch();

		void AddItemData (DWORD vid, long long price, const TSearchItemData& rItemData);
		void ClearItemData();

		DWORD GetItemDataCount()
		{
			return m_ItemInstanceVector.size();
		}
		DWORD GetItemDataPtr (DWORD index, TSearchItemData** ppInstance);

		TPlayerItemAttribute* GetItemAttr() { return m_attr; }
		void SetItemAttr(TPlayerItemAttribute* set_attr);

		long* GetItemSockets() { return m_sockets; }
		void SetItemSockets(long* set_sockets);

		DWORD GetItemTransmutation() { return m_transmutation;  }
		void SetItemTransmutation(DWORD newTransmutation) { m_transmutation = newTransmutation; }

		long GetItemKills() { return m_kills; }
		void SetItemKills(long kills) { m_kills = kills; }

	protected:
		TItemInstanceVector m_ItemInstanceVector;
		long m_sockets[ITEM_SOCKET_SLOT_MAX_NUM];
		TPlayerItemAttribute m_attr[ITEM_ATTRIBUTE_SLOT_MAX_NUM];
		DWORD m_transmutation;
		long m_kills;
};